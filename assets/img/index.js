
// More
import Background from "./background.png";

import ParticipateHumeur from "./participate_humeur.png";
import ParticipatePhoto from "./participate_photo.png";
import ParticipateVideo from "./participate_video.png";
import ParticipateQuiz from "./participate_quiz.png";

import VoteHumeur from "./vote_humeur.png";
import VotePhoto from "./vote_photo.png";
import VoteVideo from "./vote_video.png";
import VoteQuiz from "./vote_quiz.png";

import ResultHumeur from "./result_humeur.png";
import ResultPhoto from "./result_photo.png";
import ResultVideo from "./result_video.png";
import ResultQuiz from "./result_quiz.png";


import Logo from "./logo.png";

export default {
  background: Background,
  logo: Logo,
  participateHumeur: ParticipateHumeur,
  participatePhoto: ParticipatePhoto,
  participateVideo: ParticipateVideo,
  participateQuiz: ParticipateQuiz,
  voteHumeur: VoteHumeur,
  votePhoto: VotePhoto,
  voteVideo: VoteVideo,
  voteQuiz: VoteQuiz,
  resultHumeur: ResultHumeur,
  resultPhoto: ResultPhoto,
  resultVideo: ResultVideo,
  resultQuiz: ResultQuiz
};
