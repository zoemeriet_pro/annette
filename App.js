import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { LogBox, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Actions, Router, Stack, Tabs, Scene } from "react-native-router-flux";

import AppLoading from 'expo-app-loading'
import * as Font from "expo-font";

// API
import * as firebase from 'firebase';
import apiKeys from './src/config/keys';

// Components
import Icon from './src/components/atoms/Icon';

// Pages
import Register from './src/pages/auth/Register';
import RegisterFamily from './src/pages/auth/RegisterFamily';
import RegisterUser from './src/pages/auth/RegisterUser';
import Login from './src/pages/auth/Login';
import Home from './src/pages/home/Home';
import Defis from './src/pages/defis/Defis';
import DefiParticipate from './src/pages/defis/DefiParticipate';
import ParticipatePhoto from './src/pages/defis/photo/ParticipatePhoto';
import VotePhoto from './src/pages/defis/photo/VotePhoto';
import DefiVote from './src/pages/defis/DefiVote';
import DefiResult from './src/pages/defis/DefiResult';
import Souvenirs from './src/pages/souvenirs/Souvenirs';
import Profil from './src/pages/profil/Profil';
import MyFamily from './src/pages/profil/MyFamily';
import MyFamilyProfil from './src/pages/profil/MyFamilyProfil';

// Config
import colors from "./src/config/colors";
import NavBottom from './src/components/NavBottom';


LogBox.ignoreAllLogs()


export default function App() {

  const renderBackButton = () => (
    <TouchableOpacity onPress={() => Actions.pop()}>
        <View style={styles.buttonBack}>
          <Icon name="ArrowLeft" style={styles.buttonBackIcon} />
        </View>
    </TouchableOpacity>
  )

  if (!firebase.apps.length) {
    console.log('Connected with Firebase')
    firebase.initializeApp(apiKeys.firebaseConfig);
  }

  const [isReady, setIsReady] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      setIsAuthenticated(true);
    } else {
      setIsAuthenticated(false);
    }
  });

  useEffect(() => {
    const load = async () => {
      await Font.loadAsync({
        assistantLight: require("./assets/fonts/Assistant-Light.ttf"),
        assistant: require("./assets/fonts/Assistant-Regular.ttf"),
        assistantMedium: require("./assets/fonts/Assistant-Medium.ttf"),
        assistantBold: require("./assets/fonts/Assistant-Bold.ttf"),
        assistantExtraBold: require("./assets/fonts/Assistant-ExtraBold.ttf"),
        icons: require('./assets/fonts/icomoon.ttf')
      });

      setIsReady(true);
    };
    load();
  }, []);

  if (!isReady) {
    return <AppLoading />;
  }

  if(isAuthenticated) {
    return (
      <Router>
        <Tabs
          key="root"
          tabBarComponent={NavBottom}
          headerTransparent={true}
          renderBackButton={() => renderBackButton()}
        >
          <Stack key="Accueil" icon="Home">
            <Scene key="home" component={Home} initial />
          </Stack>

          <Stack key="Défis" icon="Game">
            <Scene key="defis" component={Defis} />
            <Scene key="defiParticipate" component={DefiParticipate} />
            <Scene key="participatePhoto" component={ParticipatePhoto} />
            <Scene key="votePhoto" component={VotePhoto} />
            <Scene key="defiVote" component={DefiVote} />
            <Scene key="defiResult" component={DefiResult} />
          </Stack>

          <Stack key="Souvenirs" icon="Folder">
            <Scene key="souvenirs" component={Souvenirs} />
          </Stack>

          <Stack key="Profil" icon="Profile">
            <Scene key="profil" component={Profil} initial />
            <Scene key="myFamily" component={MyFamily} />
            <Scene key="myFamilyProfil" component={MyFamilyProfil} />
          </Stack>
        </Tabs>
      </Router>
    ) 
  } else {
    return (
      <Router>
        <Stack 
          key="root"
          headerTransparent={true}
          renderBackButton={() => renderBackButton()}
        >
          <Scene key="login" component={Login} initial />
          <Scene key="register" component={Register} />
          <Scene key="registerFamily" component={RegisterFamily} />
          <Scene key="registerUser" component={RegisterUser} />
        </Stack>
      </Router>
    )
  }
    
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonBack: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 40,
    marginTop: 54,
    paddingLeft: 20,
    backgroundColor: colors.white,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  buttonBackIcon: {
    color: colors.black,
    fontSize: 25
  }
});
