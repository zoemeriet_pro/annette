import React from "react";
import { Dimensions, Text, Image, View, TouchableOpacity, StyleSheet } from "react-native";
import { Footer, FooterTab, Button, ImageBackground } from "native-base";
import { Actions } from "react-native-router-flux";
import Icon from '../components/atoms/Icon';


import Images from "../../assets/img";

// Config
import colors from "../config/colors";

export default class NavBottom extends React.Component {
  onTabClick = key => {
    Actions.jump(key);
  };

  render() {
    const { state } = this.props.navigation;
    const activeTabIndex = state.index;

    return (
      <View>
        <Footer style={styles.footer}>
          <Image source={Images["background"]} style={styles.background} />
          <FooterTab style={styles.tab}>
            {state.routes.map((element, index) => (
              <TouchableOpacity key={element.key} onPress={() => this.onTabClick(element.key)} style={styles.button}>
                <Icon
                  name={element.routes[0].params.icon ? element.routes[0].params.icon : "list-ul"}
                  style={[styles.icon, activeTabIndex === index && styles.iconAcive]}
                />
                <Text style={[styles.text, activeTabIndex === index && styles.textActive]}>{element.key}</Text>
              </TouchableOpacity>
            ))}
          </FooterTab>
        </Footer>
      </View>
    );
  }
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  footer: {
    position: "relative",
    height: width > 600 ? 90 : 65,
    backgroundColor: colors.white
  },
  background: {
    position: "absolute",
    top: -40,
    left: 0,
    width: "100%",
    resizeMode: 'stretch',
  },
  tab: {
    flex: 1
  },
  button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    fontSize: 25,
    color: colors.black
  },
  iconAcive: {
    color: colors.blue
  },
  text: {
    marginTop: 5
  },
  textActive: {
    color: colors.blue
  }
});
