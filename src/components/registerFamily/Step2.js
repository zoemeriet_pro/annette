import React from 'react';
import { View } from 'react-native';

// Components
import { TitleLogin } from "../atoms/Title";
import { ButtonGoal } from "../atoms/Button";

const Step2 = ({currentStep, familyGoal, onGoalSelect}) => {

  if(currentStep !== 2) {
    return null
  }

  const goal1 = "Se donner plus de nouvelle"
  const goal2 = "Mieux se connaître"
  const goal3 = "S'amuser en famille"

  return (
    <View>
      <TitleLogin text="L'objectif familial" />
      <View>
        <ButtonGoal text={goal1} familyGoal={familyGoal} onPress={() => onGoalSelect(goal1)} />
        <ButtonGoal text={goal2} familyGoal={familyGoal} onPress={() => onGoalSelect(goal2)} />
        <ButtonGoal text={goal3} familyGoal={familyGoal} onPress={() => onGoalSelect(goal3)} />
      </View>
    </View>
  )

}

export default Step2;