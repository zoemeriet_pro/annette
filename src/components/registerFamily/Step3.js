import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

// Components
import { TitleLogin } from "../atoms/Title";
import { ButtonGoal } from "../atoms/Button";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";
import { color } from 'react-native-reanimated';

const Step3 = ({currentStep, familyCode}) => {

  if(currentStep !== 3) {
    return null
  }

  return (
    <View>
      <TitleLogin text="Code famille" />
      <Text style={styles.body}>Partagez le code à votre famille pour leur permettre de vous rejoindre</Text>
      <View>
        <Text style={styles.code}>{familyCode}</Text>
      </View>
    </View>
  )

}

const styles = StyleSheet.create({
  body: {
    color: colors.black,
    fontFamily: typography.regular,
    fontSize: 18,
    textAlign: 'center',
    marginTop: 20
  },
  code: {
    color: colors.blue,
    fontFamily: typography.bold,
    fontSize: 30,
    textAlign: 'center',
    marginTop: 20
  }
});

export default Step3;