import React from 'react';
import { View } from 'react-native';

// Components
import { TitleLogin } from "../atoms/Title";
import { Input } from "../atoms/Input";

const Step1 = ({currentStep, name, onChangeName}) => {

  if(currentStep !== 1) {
    return null
  }

  return (
    <View>
      <TitleLogin text="Nom de la famille" />
      <View>
        <Input 
          placeholder="Nom de la famille"
          value={name}
          onChangeText={onChangeName}
        />
      </View>
    </View>
  )

}

export default Step1;