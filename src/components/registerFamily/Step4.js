import React from 'react';
import { View } from 'react-native';

// Components
import { TitleLogin } from "../atoms/Title";

const Step4 = ({currentStep}) => {

  if(currentStep !== 4) {
    return null
  }

  return (
    <View>
      <TitleLogin text="Il faut maintenant créer ton compte personnel" />
      <View>

      </View>
    </View>
  )

}

export default Step4;