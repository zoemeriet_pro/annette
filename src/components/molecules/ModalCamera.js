import React from 'react';
import { View, StyleSheet, TouchableOpacity, Modal, Dimensions } from 'react-native';
import { Camera } from 'expo-camera';

// Components
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";


export const ModalCamera = ({ visible, cameraType, reference, takePicture, switchCamera }) => {

  return (
    <Modal 
      visible={visible}
      transparent={true}
    >
      <View style={styles.cameraWrapper}>
        <Camera
          type={cameraType}
          style={styles.camera}
          ref={reference}
        >
        </Camera>
        <View style={styles.cameraActions}>
          <View style={styles.cameraHide}></View>
          <TouchableOpacity onPress={takePicture} style={styles.cameraButton}></TouchableOpacity>
          <TouchableOpacity onPress={switchCamera} style={styles.cameraSwitch}>
            <Icon name="swap" style={styles.cameraSwitchIcon} />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  cameraWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    width: width,
    height: height,
    zIndex: 2,
    backgroundColor: colors.black
  },
  camera: {
    height: width
  },
  cameraActions: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 40,
    paddingHorizontal: 20
  },
  cameraHide: {
    width: 50,
  },
  cameraButton: {
    width: 70,
    height: 70,
    borderRadius: 50,
    backgroundColor: colors.white
  },
  cameraSwitch: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#393e46'
  },
  cameraSwitchIcon: {
    color: colors.white,
    fontSize: 18
  }
});