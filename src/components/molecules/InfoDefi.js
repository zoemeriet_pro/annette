import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

// Components
import { TitleBlack } from "../atoms/Title";
import { PointsBig } from '../atoms/Points';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";


export const InfoDefi = ({ data }) => {
  return (
    <View style={styles.card}>
      <TitleBlack text={data.title} />
      <Text style={styles.body}>{data.description}</Text>
    </View>
  )
}

export const InfoDefiParticipate = ({ data }) => {
  return (
    <View style={[styles.card, styles.cardPoints]}>
      <View style={styles.points}><PointsBig number={data.points} /></View>
      <TitleBlack text={data.title} />
      <Text style={styles.body}>{data.description}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    display: 'flex',
    alignItems: 'flex-start',
    marginTop: 20,
    padding: 20,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },

  body: {
    color: colors.black,
    fontFamily: typography.regular,
    fontSize: 18,
    marginTop: 10
  },

  // Points
  cardPoints: {
    marginTop: 40,
    paddingTop: 0
  },
  points: {
    marginTop: -20,
    marginBottom: 10,
  }
});