import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

// API
import * as firebase from 'firebase'

// Components
import { Tag } from "../atoms/Tag";
import { Points } from "../atoms/Points";
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const Participation = ({ data }) => {
  const db = firebase.firestore()
  const docUser = db.collection('users').doc(data.idUser)

  const [loading, setLoading] = useState(true)
  const [dataUser, setDataUser] = useState()

  useEffect(() => {
    docUser.get().then(doc => {
      if (doc.exists) {
        const { avatar, surname } = doc.data()
        setDataUser({ avatar, surname})
        setLoading(false)
      } else {
        console.log("Error: No doc user");
      }
    })
  }, [])

  if(loading) {
    return null;
  }

  return (
    <View style={styles.card}>
      <View style={styles.avatarWrapper}>
        <Image style={styles.avatar} source={{uri: dataUser.avatar}} />
      </View>
      <Text style={styles.title}>{dataUser.surname}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: colors.blue,
    backgroundColor: colors.white,
  },
  avatarWrapper: {
    borderBottomLeftRadius: 8,
    overflow: 'hidden'
  },
  avatar: {
    width: 55,
    height: 65,
    marginTop: 10,
    marginLeft: -10,
    marginBottom: -10,
    marginRight: 10,
    resizeMode: 'contain',
  },
  title: {
    color: colors.black,
    fontSize: 18,
    fontFamily: typography.medium
  }
});