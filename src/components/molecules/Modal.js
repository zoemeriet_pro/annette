import React from "react";
import { Modal, View, Dimensions, Text, StyleSheet, Pressable, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";

// API
import {loggingOut} from '../../api/firebaseMethods';

// Components
import Icon from '../atoms/Icon';
import { ButtonBorder } from '../atoms/Button'

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const ModalProfil = ({visible, onClose}) => {

  return (
    <Modal
      visible={visible}
      transparent={true}
    >
      <Pressable onPress={onClose} style={styles.background}></Pressable>
      
      <View style={styles.modal}>
        <View style={styles.modalWrapperLine}><View style={styles.modalLine}></View></View>
        <View style={styles.modalContent}>
          <Pressable 
            style={styles.modalLink} 
            onPressIn={() => {Actions.myFamily()}}
            onPress={onClose}
          >
            <Icon name="User1" style={styles.modalLinkIcon} />
            <Text style={styles.modalLinkText}>Voir ma famille</Text>
          </Pressable>

          <Pressable 
            style={styles.modalLink}
            onPressIn={() => {loggingOut()}}
            onPress={onClose}
          >
            <Icon name="Logout" style={styles.modalLinkIcon} />
            <Text style={styles.modalLinkText}>Déconnexion</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  )
}

export const ModalImagePicker = ({visible, onClose, takePhotoFromCamera, choosePhotoFromLibrary}) => {

  return (
    <Modal
      visible={visible}
      transparent={true}
    >
      <Pressable onPress={onClose} style={styles.background}></Pressable>
      
      <View style={styles.imagePicker}>
        <View style={styles.imagePickerWrapper}>
          <Text style={styles.imagePickerText}>Que voulez-vous faire ?</Text>
          <TouchableOpacity style={styles.imagePickerButton} onPress={takePhotoFromCamera}>
            <Text style={styles.imagePickerButtonText}>Prendre une photo</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.imagePickerButton} onPress={choosePhotoFromLibrary}>
            <Text style={styles.imagePickerButtonText}>Choisir une photo existante</Text>
          </TouchableOpacity>
        </View>
        <ButtonBorder text="Annuler" onPress={onClose} />
      </View>
    </Modal>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  background: {
    width: width,
    height: height,
    backgroundColor: colors.black,
    opacity: 0.5
  },
  modal: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: width,
    paddingTop: 20,
    paddingBottom: 30,
    paddingHorizontal: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: "white",
  },
  modalWrapperLine: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 10
  },
  modalLine: {
    width: 48,
    height: 3,
    borderRadius: 10,
    backgroundColor: colors.grey
  },
  modalLink: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.background,
    paddingTop: 10,
    paddingBottom: 20,
    marginBottom: 10
  },
  modalLinkIcon: {
    width: 20,
    fontSize: 14
  },
  modalLinkText: {
    marginLeft: 10,
    fontSize: 18,
    fontFamily: typography.regular,
  },

  // IMAGE PICKER
  imagePicker: {
    position: 'absolute',
    left: 0,
    bottom: 40,
    width: width,
    paddingHorizontal: width > 600 ? 50 : 20,
  },
  imagePickerWrapper: {
    paddingTop: 8,
    borderWidth: 1,
    borderColor: colors.blue,
    borderRadius: 10,
    backgroundColor: colors.white,
  },
  imagePickerText: {
    color: colors.grey,
    textAlign: 'center',
    marginBottom: 8
  },
  imagePickerButton: {
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderTopWidth: 1,
    borderTopColor: colors.background
  },
  imagePickerButtonText: {
    color: colors.blue,
    fontFamily: typography.medium,
    fontSize: 14,
    textTransform: "uppercase",
    textAlign: "center"
  }
});