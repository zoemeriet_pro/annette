import React, { useState } from 'react';
import { Actions } from "react-native-router-flux";
import { View, TouchableOpacity, Text, StyleSheet, Alert, Dimensions } from 'react-native';

// Components
import { TagBig } from "../atoms/Tag";
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Topbar = ({title, tag, iconRight, onPress}) => {
  return (
    <View style={styles.topbar}>
      <View style={styles.hiddenArrow}></View>

      {title &&
        <Text style={styles.title}>{title}</Text>
      }

      {tag &&
        <TagBig text={tag} />
      }

      {iconRight
        ? <TouchableOpacity onPress={onPress} style={styles.buttonHeader}><Icon name={iconRight} style={styles.buttonHeaderIcon} /></TouchableOpacity>
        : <View style={styles.buttonHide}></View>
      }
    </View>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  topbar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: width > 600 ? 88 : 66,
    paddingTop: width > 600 ? 30 : 0,
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  hiddenArrow: {
    width: 40,
  },
  title: {
    color: colors.black,
    fontSize: 16,
    fontFamily: typography.medium,
  },
  // BUTTON HEADER
  buttonHeader: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  buttonHeaderIcon: {
    fontSize: 20
  },

  buttonHide: {
    width: 40,
    height: 40,
  }
});

export default Topbar;