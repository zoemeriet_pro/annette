import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

// API
import * as firebase from 'firebase'

// Components
import { Tag } from "../atoms/Tag";
import { Points } from "../atoms/Points";
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const CardPost = ({ data }) => {
  const db = firebase.firestore()
  const docUser = db.collection('users').doc(data.idUser)

  const [loading, setLoading] = useState(true)
  const [dataUser, setDataUser] = useState()

  useEffect(() => {
    docUser.get().then(doc => {
      if (doc.exists) {
        const { avatar, surname } = doc.data()
        setDataUser({ avatar, surname})
        setLoading(false)
      } else {
        console.log("Error: No doc user");
      }
    })
  }, [])

  if(loading) {
    return null;
  }

  return (
    <View style={styles.card}>
      <View style={styles.header}>
        <View style={styles.avatar}>
          <Image style={styles.avatarImage} source={{uri: dataUser.avatar}} />
        </View>
        <View>
          <Text style={styles.title}>{dataUser.surname}</Text>
          <Text style={styles.date}>{data.date.toDate().toLocaleDateString()}</Text>
        </View>
      </View>
      <Image
        source={{ uri: data.image }}
        style={styles.image}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
    padding: 20,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    width: 43,
    height: 43,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: colors.blue,
    marginRight: 10,
  },
  avatarImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    borderRadius: 50,
  },
  title: {
    fontSize: 18,
    fontFamily: typography.bold,
    color: colors.black
  },
  date: {
    color: colors.grey
  },
  image: {
    marginTop: 20,
    width: '100%',
    height: 280,
    resizeMode: "cover",
  },
});