import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const CardProfil = ({data}) => {
  return (
    <View style={styles.top}>
      <View style={styles.topWrapperImage}>
        <Image style={styles.topImage} source={{uri: data.avatar}} />
      </View>
      
      <View style={styles.topContent}>
        <Text style={styles.topSurname}>{data.surname}</Text>
        <Text style={styles.topName}>{data.firstName} {data.lastName}</Text>
        <View style={styles.textIcon}>
          <Icon name="Calendar" style={styles.textIconIcon} />
          <Text style={styles.textIconText}>{data.birth}</Text>
        </View>
        <View style={styles.textIcon}>
          <Icon name="Message" style={styles.textIconIcon} />
          <Text style={styles.textIconText}>{data.email}</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  top: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginHorizontal: 20,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  topContent: {
    padding: 20
  },
  topWrapperImage: {
    borderBottomLeftRadius: 10,
    overflow: 'hidden'
  },
  topImage: {
    width: 100,
    height: 140,
    marginLeft: -10,
  },
  topSurname: {
    fontSize: 22,
    fontFamily: typography.bold,
    color: colors.black
  },
  topName: {
    fontSize: 16,
    fontFamily: typography.medium,
    color: colors.grey
  },

  textIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textIconIcon: {
    width: 18,
    fontSize: 12
  },
  textIconText: {
    marginLeft: 10,
    fontSize: 18,
    fontFamily: typography.regular,
  }
});

export default CardProfil;