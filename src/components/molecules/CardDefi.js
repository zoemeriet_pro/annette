import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

// Components
import { Tag } from "../atoms/Tag";
import { Points } from "../atoms/Points";
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const color = text => {
  if (text == "Photo") {
    return colors.photo;
  }
  if (text == "Video") {
    return colors.video;
  }
  if (text == "Humeur") {
    return colors.humeur;
  }
  if (text == "Quiz") {
    return colors.quiz;
  }
  return colors.white;
};

export const CardDefi = ({ data, onPress = () => {} }) => {
  const [date, setDate] = useState()

  useEffect(() => {
    if (data.dateVote) {
      setDate(data.dateVote.toDate().toLocaleDateString())
    }
    if (data.dateResult) {
      setDate(data.dateResult.toDate().toLocaleDateString())
    }
  }, [])

  return (
    <TouchableOpacity onPress={onPress} style={[styles.card, { borderColor: color(data.type) }]}>
      <View style={styles.cardTop}>
        <Tag text={data.type} />
        <Points number={data.points} />
      </View>
      <Text style={styles.cardTitle}>{data.title}</Text>
      {date &&
        <Text style={styles.cardDate}>Jusqu'au {date}</Text>
      }
    </TouchableOpacity>
  )
}

export const CardDefiHome = ({ data, onPress = () => {} }) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.card, styles.cardHome, { borderColor: color(data.type) }]}>
      <View style={styles.cardTop}>
        <Tag text={data.type} />
      </View>
      <Text style={styles.cardTitle}>{data.title}</Text>
      <Text style={styles.cardDate}>Il reste 2 jours</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
    padding: 20,
    backgroundColor: colors.white,
    borderWidth: 2,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1
  },
  cardTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  cardTitle: {
    marginTop: 20,
    fontSize: 22,
    fontFamily: typography.bold,
    color: colors.black
  },
  cardDate: {
    marginTop: 20,
    fontSize: 12,
    textTransform: 'uppercase',
    color: colors.grey
  },

  // Home
  cardHome: {
    width: 220,
    marginRight: 10
  }
});