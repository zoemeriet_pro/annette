import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Alert} from 'react-native';

// Components
import { TitleLogin } from "../atoms/Title";
import { Input } from "../atoms/Input";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Step1 = ({currentStep, email, onChangeEmail, password, onChangePassword, password2, onChangePassword2}) => {

  if(currentStep !== 1) {
    return null
  }

  return (
    <View>
      <TitleLogin text="Identifiez-vous" />
      <Text style={styles.body}>Renseignez ici votre email ainsi que votre mot de passe personnel</Text>
      <View>
        <Input 
          placeholder="Email*"
          value={email}
          onChangeText={onChangeEmail}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Input 
          placeholder="Mot de passe*"
          value={password}
          onChangeText={onChangePassword}
          secureTextEntry={true}
        />
        
        <Input 
          placeholder="Confirmer le mot de passe*"
          value={password2}
          onChangeText={onChangePassword2}
          secureTextEntry={true}
        />
      </View>
    </View>
  )

}

const styles = StyleSheet.create({
  body: {
    color: colors.black,
    fontFamily: typography.regular,
    fontSize: 18,
    textAlign: 'center',
    marginTop: 20
  }
});

export default Step1;