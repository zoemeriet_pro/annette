import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Alert} from 'react-native';

// Components
import { TitleLogin } from "../atoms/Title";
import { Input } from "../atoms/Input";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Step2 = ({currentStep, firstName, onChangeFirstName, lastName, onChangeLastName, surname, onChangeSurname}) => {

  if(currentStep !== 2) {
    return null
  }

  return (
    <View>
      <TitleLogin text="Votre nom" />
      <View>
        <Input 
          placeholder="Prénom*"
          value={firstName}
          onChangeText={onChangeFirstName}
        />
        <Input 
          placeholder="Nom*"
          value={lastName}
          onChangeText={onChangeLastName}
        />
        <Input 
          placeholder="Surnom"
          value={surname}
          onChangeText={onChangeSurname}
        />
      </View>
    </View>
  )

}
export default Step2;