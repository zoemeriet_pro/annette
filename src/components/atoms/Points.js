import React from "react";
import { View, Text, StyleSheet } from "react-native";

// Components
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const Points = ({ number }) => (
  <View style={styles.points}>
    <Text style={styles.number}>+ {number}</Text>
    <Icon name="Star1" style={styles.icon} />
  </View>
);

export const PointsBig = ({ number }) => (
  <View style={[styles.points, styles.pointsBig]}>
    <Text style={[styles.number, styles.numberBig]}>+ {number}</Text>
    <Icon name="Star1" style={[styles.icon, styles.iconBig]} />
  </View>
);

const styles = StyleSheet.create({
  // POINTS
  points: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  number: {
    marginRight: 5,
    color: colors.black,
    fontFamily: typography.bold,
    fontSize: 18,
  },
  icon: {
    color: colors.black,
    fontSize: 16
  },

  // POINTS BIG
  pointsBig: {
    paddingHorizontal: 14,
    paddingVertical: 8,
    borderRadius: 10,
    backgroundColor: colors.blue,
  },
  numberBig: {
    color: colors.white,
  },
  iconBig: {
    color: colors.white,
  }
});