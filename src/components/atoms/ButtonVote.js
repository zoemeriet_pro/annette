import React from "react";
import { View, TouchableOpacity, Text, StyleSheet, Image } from "react-native";

// Assets
import Images from "../../../assets/img";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const ButtonVotePhoto = ({ image, id, selectedImage, onPress = () => {} }) => (
  <TouchableOpacity onPress={onPress} style={[styles.button, selectedImage === id && styles.buttonActive]}>
    <Image
      source={{ uri: image }}
      style={styles.image}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  // BUTTON
  button: {
    padding: 10,
    marginBottom: 10,
    borderRadius: 10,
    backgroundColor: colors.white,
    width: '48%',
    height: 200,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1
  },
  buttonActive: {
    backgroundColor: colors.blue,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
});