import React from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import Icon from '../atoms/Icon';

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const Button = ({ text, onPress = () => {} }) => (
  <TouchableOpacity onPress={onPress} style={styles.button}>
    <Text style={styles.buttonText}>{text}</Text>
  </TouchableOpacity>
);

export const ButtonBorder = ({ text, onPress = () => {} }) => (
  <TouchableOpacity onPress={onPress} style={[styles.button, styles.buttonBorder]}>
    <Text style={[styles.buttonText, styles.buttonBorderText]}>{text}</Text>
  </TouchableOpacity>
);

export const ButtonGoal = ({ text, familyGoal, onPress = () => {} }) => (
  <TouchableOpacity onPress={onPress} style={[styles.buttonGoal, familyGoal === text && styles.buttonGoalActive]}>
    <Text style={[styles.buttonGoalText, familyGoal === text && styles.buttonGoalTextActive]}>{text}</Text>
  </TouchableOpacity>
);

export const ButtonQuiz = ({ text, onPress = () => {} }) => (
  <TouchableOpacity onPress={onPress} style={styles.buttonQuiz}>
    <Text style={styles.text}>{text}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  // BUTTON
  button: {
    marginTop: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: colors.blue,
    borderRadius: 10,
    backgroundColor: colors.blue,
    marginRight: 2
  },
  buttonText: {
    color: colors.white,
    fontFamily: typography.medium,
    fontSize: 14,
    textTransform: "uppercase",
    textAlign: "center"
  },

  // BUTTON BORDER
  buttonBorder: {
    backgroundColor: colors.white,
  },
  buttonBorderText: {
    color: colors.blue,
  },

  // BUTTON GOAL
  buttonGoal: {
    marginTop: 20,
    padding: 20,
    borderWidth: 2,
    borderColor: colors.black,
    borderRadius: 10,
    backgroundColor: colors.white,
  },
  buttonGoalText: {
    color: colors.black,
    fontFamily: typography.medium,
    fontSize: 18,
    textAlign: "center"
  },
  buttonGoalActive: {
    borderColor: colors.blue,
  },
  buttonGoalTextActive: {
    color: colors.blue
  },

  // BUTTON QUIZ
  buttonQuiz: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    borderWidth: 2,
		borderColor: 'red',
    padding: 15,
    marginTop: 20
  },
  text: {
    color: "#000",
    fontSize: 20,
    textAlign: "center"
  },
});