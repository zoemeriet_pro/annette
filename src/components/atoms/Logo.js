import React from "react";
import { View, StyleSheet, Image } from "react-native";

// Assets
import Images from "../../../assets/img";

export const LogoBig = () => (
  <View style={styles.logoBigContainer}>
    <Image source={Images["logo"]} style={styles.logoBig} />
  </View>
);

const styles = StyleSheet.create({
  logoBigContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  logoBig: {
    resizeMode: "contain",
    width: 250,
  },
});