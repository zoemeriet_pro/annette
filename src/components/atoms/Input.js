import React from "react";
import { View, TouchableOpacity, Text, TextInput, StyleSheet } from "react-native";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const Input = ({ placeholder, value, onChangeText, keyboardType, autoCapitalize, secureTextEntry }) => (
  <TextInput
    style={styles.input}
    placeholder={placeholder}
    value={value}
    onChangeText={onChangeText}
    keyboardType={keyboardType}
    autoCapitalize={autoCapitalize}
    secureTextEntry={secureTextEntry}
  />
);

const styles = StyleSheet.create({
  input: {
    marginTop: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: colors.black,
    borderRadius: 10,
    backgroundColor: colors.white,
  }
});