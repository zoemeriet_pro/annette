import React from "react";
import { Dimensions, View, TouchableOpacity, Text, StyleSheet } from "react-native";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

export const TitleLogin = ({ text }) => (
  <Text style={styles.titleLogin}>{text}</Text>
);

export const TitleBlue = ({ text }) => (
  <Text style={styles.titleBlue}>{text}</Text>
);

export const TitleBlack = ({ text }) => (
  <Text style={styles.titleBlack}>{text}</Text>
);

export const Subtitle = ({ text }) => (
  <Text style={styles.subtitle}>{text}</Text>
);

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  titleLogin: {
    color: colors.black,
    fontFamily: typography.bold,
    fontSize: 30,
    textTransform: "uppercase",
    textAlign: "center"
  },

  titleBlue: {
    color: colors.blue,
    fontFamily: typography.bold,
    fontSize: width > 600 ? 45 : 38,
  },

  titleBlack: {
    color: colors.black,
    fontFamily: typography.bold,
    fontSize: width > 600 ? 36 : 30,
  },

  subtitle: {
    color: colors.black,
    fontFamily: typography.medium,
    fontSize: 25,
    marginTop: 10
  },
});