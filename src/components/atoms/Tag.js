import React from "react";
import { View, Text, StyleSheet } from "react-native";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const color = text => {
  if (text == "Photo") {
    return colors.photo;
  }
  if (text == "Video") {
    return colors.video;
  }
  if (text == "Humeur") {
    return colors.humeur;
  }
  if (text == "Quiz") {
    return colors.quiz;
  }
  return colors.white;
};

export const Tag = ({ text }) => (
  <View style={[styles.tag, { backgroundColor: color(text) }]}>
    <Text style={styles.tagText}>{text}</Text>
  </View>
);

export const TagBig = ({ text }) => (
  <View style={[styles.tagBig, { backgroundColor: color(text) }]}>
    <Text style={styles.tagBigText}>{text}</Text>
  </View>
);

const styles = StyleSheet.create({
  // TAG
  tag: {
    paddingHorizontal: 18,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: colors.blue,
  },
  tagText: {
    color: colors.black,
    fontFamily: typography.medium,
    fontSize: 12,
    textAlign: "center"
  },

  // TAG BIG
  tagBig: {
    paddingHorizontal: 20,
    paddingVertical: 7,
    borderRadius: 10,
  },
  tagBigText: {
    color: colors.black,
    fontFamily: typography.medium,
    fontSize: 18,
    textAlign: "center"
  },

  // Color
  Photo: {
    backgroundColor: colors.photo
  }
});