import * as firebase from "firebase";
import "firebase/firestore";
import {Alert} from "react-native";

export async function registration(email, password, lastName, firstName, surname, familyCode) {
  try {
    await firebase.auth().createUserWithEmailAndPassword(email, password);
    const currentUser = firebase.auth().currentUser;

    const db = firebase.firestore();
    db.collection("users")
      .doc(currentUser.uid)
      .set({
        email: currentUser.email,
        lastName: lastName,
        firstName: firstName,
        surname: surname,
        familyCode: familyCode
      });
  } catch (err) {
    Alert.alert("There is something wrong!!!!", err.message);
  }
}

export async function login(email, password) {
  try {
   await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);
  } catch (err) {
    Alert.alert("There is something wrong!", err.message);
  }
}

export async function loggingOut() {
  try {
    await firebase.auth().signOut();
  } catch (err) {
    Alert.alert('There is something wrong!', err.message);
  }
}

export async function registerFamily(name, goal, code) {
  try {
    const db = firebase.firestore();
    db.collection("family")
      .doc(code)
      .set({
        name: name,
        goal: goal,
        code: code
      });
  } catch (err) {
    Alert.alert("There is something wrong!!!!", err.message);
  }
}

export async function addDefiPhoto(idUser, idDefi, imageURL) {
  try {
    const db = firebase.firestore()
    db.collection("participations")
      .doc(idUser + idDefi)
      .set({
        idUser: idUser,
        idDefi: idDefi,
        image: imageURL,
        date: firebase.firestore.FieldValue.serverTimestamp()
      })
  } catch (err) {
    Alert.alert("There is something wrong!!!!", err.message);
  }
}

export async function addVotePhoto(idUser, idDefi, vote) {
  try {
    const db = firebase.firestore()
    db.collection("votes")
      .doc(idUser + idDefi)
      .set({
        idUser: idUser,
        idDefi: idDefi,
        vote: vote,
      })
  } catch (err) {
    Alert.alert("There is something wrong!!!!", err.message);
  }
}
