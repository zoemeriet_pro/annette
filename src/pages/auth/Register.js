import React, { useEffect, useState } from 'react';
import { Actions } from "react-native-router-flux";
import { View, Text, StyleSheet, Alert, ScrollView, SafeAreaView, Dimensions } from 'react-native';

// API
import * as firebase from 'firebase';
// import { getFamilyCodes } from '../../api/firebaseMethods';

// Components
import { TitleLogin } from "../../components/atoms/Title";
import { Button } from "../../components/atoms/Button";
import { Input } from "../../components/atoms/Input";

// Config
import colors from "../../config/colors";


const Register = () => {

  const [familyCodes, setFamilyCodes] = useState([])
  const [familyCode, setFamilyCode] = useState('')

  useEffect(() => {
    async function getFamilyCodes() {
      let db = await firebase.firestore()
      db.collection("family").get().then((querySnapshot) => {
        var list = []
        querySnapshot.forEach(doc => {
          list.push(doc.id)
        })
        setFamilyCodes(list)
      })
    }
    getFamilyCodes()
  })

  const goToRegisterFamily = () => {
    Actions.registerFamily();
  };

  const goToRegisterUser = () => {
    if(!familyCode) {
      Alert.alert('Le code famille est obligatoire');
    } else {
      if(familyCodes.includes(familyCode)) {
        console.log(familyCode);
        Actions.registerUser({familyCode: familyCode})
      } else {
        Alert.alert('Le code ne correspond à aucune famille');
      }
    }
  };

  return (
    <SafeAreaView style={styles.page}>
      <ScrollView>
        <View style={styles.container}>

          <View style={styles.wrapper}>
            <TitleLogin text="Rejoins ta famille" />
            <Input 
              placeholder="Code famille"
              value={familyCode}
              onChangeText={(familyCode) => setFamilyCode(familyCode)}
            />
            <Button text="Suivant" onPress={goToRegisterUser} />
          </View>

          <View style={styles.lineContainer}><View style={styles.line}></View></View>

          <View style={styles.wrapper}>
            <TitleLogin text="Pas encore de compte famille ?" />
            <Button text="Je crée mon compte famille" onPress={goToRegisterFamily} />
          </View>

        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    paddingTop: 60,
    paddingHorizontal: 20,
  },
  wrapper: {
    marginVertical: 80
  },
  lineContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  line: {
    width: 200,
    height: 2,
    backgroundColor: colors.black
  }
});

export default Register;