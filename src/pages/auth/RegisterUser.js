import React, { useState } from 'react';
import { View, Text, TextInput, Alert, ScrollView, Keyboard, StyleSheet, SafeAreaView, Dimensions} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Actions } from 'react-native-router-flux';

// API
import { registration } from '../../api/firebaseMethods';

// Components
import { Button, ButtonBorder } from "../../components/atoms/Button";

// Steps
import Step1 from "../../components/registerUser/Step1";
import Step2 from "../../components/registerUser/Step2";

// Config
import colors from "../../config/colors";

const RegisterUser = ({familyCode}) => {
  const [currentStep, setCurrentStep] = useState(1);

  console.log(familyCode);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [surname, setSurname] = useState('');

  const emptyState = () => {
    setEmail('');
    setPassword('');
    setConfirmPassword('');
    setFirstName('');
    setLastName('');
    setSurname('');
  };

  const nextStep = () => {
    // Step 1
    if(currentStep == 1) {
      if(!email) {
        Alert.alert("Email est obligatoire");
      } else if(!password) {
        Alert.alert("Mot de passe est obligatoire");
      } else if(!confirmPassword) {
        Alert.alert("Confirmer mot de passe est obligatoire");
      } else if(password !== confirmPassword) {
        Alert.alert('Les mots de passe de sont pas les mêmes');
      } else {
        setCurrentStep(currentStep + 1)
      }
    }
    // Step 2
    if(currentStep == 2) {
      if (!firstName) {
        Alert.alert("Prénom est obligatoire");
      } else if(!lastName) {
        Alert.alert("Nom est obligatoire");
      } else {
        registration(
          email,
          password,
          lastName,
          firstName,
          surname,
          familyCode
        );
        Actions.home();
        emptyState();
      }
    }
  }

  const prevStep = () => {
    setCurrentStep(currentStep - 1)
  }

  return (
    <SafeAreaView>
      <View style={styles.page}>
        <ScrollView onBlur={Keyboard.dismiss}>

        <Step1 
          currentStep={currentStep} 
          email={email}
          onChangeEmail={(email) => setEmail(email)} 
          password={password}
          onChangePassword={(password) => setPassword(password)}
          password2={confirmPassword}
          onChangePassword2={(password2) => setConfirmPassword(password2)}
        />
        <Step2 
          currentStep={currentStep} 
          firstName={firstName}
          onChangeFirstName={(firstName) => setFirstName(firstName)}
          lastName={lastName}
          onChangeLastName={(lastName) => setLastName(lastName)}
          surname={surname}
          onChangeSurname={(surname) => setSurname(surname)}
        />

        <View style={styles.actions}>
          <View style={styles.actionLeft}><ButtonBorder text="Retour" onPress={prevStep} /></View>
          <View style={styles.actionRight}><Button text="Suivant" onPress={nextStep} /></View>
        </View>

       </ScrollView>
     </View>
    </SafeAreaView>
  );
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    paddingTop: 60,
    paddingHorizontal: 20,
    minHeight: height,
    backgroundColor: colors.background
  },
  actions: {
    display: 'flex',
    flexDirection: "row",
  },
  actionLeft: {
    flex: 1,
    marginRight: 5,
  },
  actionRight: {
    flex: 1,
    marginLeft: 5,
  }
});

export default RegisterUser;