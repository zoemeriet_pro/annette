import React, { useState } from 'react';
import { Actions } from "react-native-router-flux";
import { View, Text, StyleSheet, Alert, Dimensions, ScrollView, SafeAreaView } from 'react-native';

// API
import * as firebase from 'firebase';
import { login } from '../../api/firebaseMethods';

// Components
import { Button } from "../../components/atoms/Button";
import { Input } from "../../components/atoms/Input";
import { LogoBig } from "../../components/atoms/Logo";

// Assets
import colors from "../../config/colors";

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const goToRegister = () => {
    Actions.register();
  };

  const goToHome = () => {
    if (!email) {
      Alert.alert('Email field is required.');
    } else if (!password) {
      Alert.alert('Password field is required.');
    } else {
      login(email, password);
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) Actions.home();
      });
    }
    setEmail('');
    setPassword('');
  };

  return (
    <SafeAreaView style={styles.page}>
      <ScrollView>

        <View style={styles.container}>
          <LogoBig />

          <View style={styles.login}>
            <Input 
              placeholder="Email"
              value={email}
              onChangeText={(email) => setEmail(email)}
              autoCapitalize="none"
            />
            <Input 
              placeholder="Mot de passe"
              value={password}
              onChangeText={(password) => setPassword(password)}
              secureTextEntry={true}
            />
            <Button text="Connexion" onPress={goToHome} />
            <Text style={styles.forgetPassword}>Mot de passe oublié</Text>
          </View>
  
          <Button text="Je m'inscris" onPress={goToRegister} />
        </View>

      </ScrollView>
    </SafeAreaView>
  );
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    paddingTop: 80,
    paddingHorizontal: width > 600 ? 50 : 20,
  },
  login: {
    marginTop: 30,
    marginBottom: 60
  },
  forgetPassword: {
    marginTop: 20,
    textAlign: 'right',
    textDecorationLine: 'underline'
  }
});

export default Login;