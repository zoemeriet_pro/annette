import React, { useState } from 'react';
import { Actions } from "react-native-router-flux";
import { View, StyleSheet, Alert, Dimensions } from 'react-native';

// API
import * as firebase from 'firebase';
import { registerFamily } from '../../api/firebaseMethods';

// Components
import { TitleLogin } from "../../components/atoms/Title";
import { Button, ButtonBorder } from "../../components/atoms/Button";
import { Input } from "../../components/atoms/Input";
import Topbar from "../../components/molecules/Topbar";

// Steps
import Step1 from "../../components/registerFamily/Step1";
import Step2 from "../../components/registerFamily/Step2";
import Step3 from "../../components/registerFamily/Step3";
import Step4 from "../../components/registerFamily/Step4";

// Config
import colors from "../../config/colors";
import randomCode from "../../config/randomCode";

const RegisterFamily = () => {
  const [currentStep, setCurrentStep] = useState(1);

  const [familyCode, setFamilyCode] = useState(randomCode());
  const [familyName, setFamilyName] = useState('');
  const [familyGoal, setFamilyGoal] = useState('');

  const nextStep = () => {
    // Step 1
    if(currentStep == 1) {
      if(!familyName) {
        Alert.alert("Le nom de ta famille est obligatoire")
      } else {
        setCurrentStep(currentStep + 1)
      }
    }
    // Step 2
    if(currentStep == 2) {
      if(!familyName) {
        Alert.alert("Tu dois séléctionner un objectif")
      } else {
        setCurrentStep(currentStep + 1)
      }
    }
    // Step 3
    if(currentStep == 3) {
      setCurrentStep(currentStep + 1)
    }
    // Step 4
    if(currentStep == 4) {
      registerFamily(familyName, familyGoal, familyCode)
      Actions.registerUser({familyCode: familyCode})
    }
  }

  const prevStep = () => {
    if(currentStep == 1) {
      Actions.register()
    } else {
      setCurrentStep(currentStep - 1)
    }
  }


  return (
    <View style={styles.page}>

      <Step1 
        currentStep={currentStep} 
        name={familyName}
        onChangeName={(familyName) => setFamilyName(familyName)} 
      />

      <Step2 
        currentStep={currentStep} 
        familyGoal={familyGoal}
        onGoalSelect={goal => setFamilyGoal(goal)}
      />
      
      <Step3 
        currentStep={currentStep}
        familyCode={familyCode}
      />
      
      <Step4
        currentStep={currentStep}
      />

      <View style={styles.actions}>
        <View style={styles.actionLeft}><ButtonBorder text="Retour" onPress={prevStep} /></View>
        <View style={styles.actionRight}><Button text="Suivant" onPress={nextStep} /></View>
      </View>
    </View>
  );
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    paddingTop: 60,
    paddingHorizontal: 20,
    minHeight: height,
    backgroundColor: colors.background
  },
  actions: {
    display: 'flex',
    flexDirection: "row",
  },
  actionLeft: {
    flex: 1,
    marginRight: 5,
  },
  actionRight: {
    flex: 1,
    marginLeft: 5,
  }
});

export default RegisterFamily;