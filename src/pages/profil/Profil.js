import React, { useEffect, useState } from "react";
import { StyleSheet, View, ScrollView, SafeAreaView, Text, Dimensions, Image } from "react-native";
import { color } from "react-native-reanimated";
import { Actions } from "react-native-router-flux";

// API
import * as firebase from 'firebase';

// Components
import { ModalProfil } from "../../components/molecules/Modal";
import Topbar from "../../components/molecules/Topbar";
import CardProfil from "../../components/molecules/CardProfil";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Profil = () => {
  const db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid;
  const docUser = db.collection('users').doc(currentUserUID)

  const [loading, setLoading] = useState(true)
  const [modalVisible, setModalVisible] = useState(false)
  const [dataUser, setDataUser] = useState()

  useEffect(() => {
    docUser.get().then(doc => {
      if (doc.exists) {
        const { firstName, lastName, surname, email, birth, avatar } = doc.data()
        setDataUser({ firstName, lastName, surname, email, birth, avatar })
        setLoading(false)
      } else {
        console.log("Error: No doc user");
      }
    })
  }, [])

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>
      <Topbar title="Mon profil" iconRight="More" onPress={() => {setModalVisible(true)}} />

      <ScrollView>

        <CardProfil data={dataUser} />

      </ScrollView>

      <ModalProfil 
        visible={modalVisible}
        onClose={() => setModalVisible(false)}
      />
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
});


export default Profil;