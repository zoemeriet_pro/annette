import React, { useEffect, useState }  from "react";
import { StyleSheet, View, ScrollView, SafeAreaView, Text, Dimensions, TouchableOpacity, Image } from "react-native";

// API
import * as firebase from 'firebase';

// Components
import Topbar from "../../components/molecules/Topbar";
import Icon from "../../components/atoms/Icon";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";
import { Actions } from "react-native-router-flux";

const MyFamily = () => {
  const db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid;
  const docUser = db.collection('users').doc(currentUserUID)

  const [loading, setLoading] = useState(true)
  const [dataFamily, setDataFamily] = useState()

  const getDataFamily = (familyCode) => {
    const docFamily = db.collection('users').where('familyCode', '==', familyCode).orderBy("surname")
    docFamily.get().then(querySnapshot => {
      var list = []
      querySnapshot.forEach(doc => {
        if (doc.id != currentUserUID) {
          const id = doc.id
          const { surname, firstName, lastName, avatar } = doc.data()
          list.push({ id, surname, firstName, lastName, avatar })
        }
      })
      setDataFamily(list)
      setLoading(false)
    })
  }

  useEffect(() => {
    docUser.get().then(doc => {
      if (doc.exists) {
        const { familyCode } = doc.data()
        getDataFamily(familyCode)
      } else {
        console.log("Error: No doc user");
      }
    })
  }, [])

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>
      <Topbar title="Ma famille" iconRight="AddUser" />
      <ScrollView>

        <View style={styles.list}>
          {dataFamily.map((item, index) => (
            <TouchableOpacity key={index} style={styles.card} onPress={() => {Actions.myFamilyProfil({id: item.id})}}>
              <View style={styles.cardWrapperImage}>
                <Image style={styles.cardImage} source={{uri: item.avatar}} />
              </View>
              <View style={styles.cardContent}>
                <Text style={styles.cardTitle}>{item.surname}</Text>
                <Text style={styles.cardSubtitle}>{item.firstName} {item.lastName}</Text>
              </View>
              <Icon name="ArrowRight" style={styles.cardIcon} />
            </TouchableOpacity>
          ))}
        </View>

      </ScrollView>
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },

  list: {
    paddingHorizontal: 20
  },

  card: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  cardWrapperImage: {
    borderBottomLeftRadius: 10,
    overflow: 'hidden'
  },
  cardImage: {
    width: 68,
    height: 100,
    marginTop: 10,
    marginLeft: -10,
  },
  cardContent: {
    flex: 1,
    marginLeft: 20
  },
  cardTitle: {
    fontSize: 22,
    fontFamily: typography.bold
  },
  cardSubtitle: {
    fontSize: 18,
    fontFamily: typography.regular,
    color: colors.grey
  },
  cardIcon: {
    fontSize: 22,
    marginRight: 20
  }
});


export default MyFamily;