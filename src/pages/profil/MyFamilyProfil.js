import React, { useEffect, useState } from "react";
import { StyleSheet, View, ScrollView, SafeAreaView, Text, Dimensions } from "react-native";

// API
import * as firebase from 'firebase';

// Components
import Topbar from "../../components/molecules/Topbar";
import CardProfil from "../../components/molecules/CardProfil";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const MyFamilyProfil = ({ id }) => {
  const db = firebase.firestore()
  const docUser = db.collection('users').doc(id)

  const [loading, setLoading] = useState(true)
  const [dataUser, setDataUser] = useState()

  useEffect(() => {
    docUser.get().then(doc => {
      if (doc.exists) {
        const { firstName, lastName, surname, email, birth, avatar } = doc.data()
        setDataUser({ firstName, lastName, surname, email, birth, avatar })
        setLoading(false)
      } else {
        console.log("Error: No doc user");
      }
    })
  }, [])

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>
      <Topbar title={dataUser.surname} />

      <ScrollView>
        <CardProfil data={dataUser} />
      </ScrollView>
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },

  top: {
    marginHorizontal: 20,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  topContent: {
    padding: 20
  },
  topSurname: {
    fontSize: 22,
    fontFamily: typography.medium,
    color: colors.black
  },
  topName: {
    fontSize: 16,
    fontFamily: typography.medium,
    color: colors.grey
  },

  textIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textIconIcon: {
    width: 18,
    fontSize: 12
  },
  textIconText: {
    marginLeft: 10,
    fontSize: 18,
    fontFamily: typography.regular,
  }
});


export default MyFamilyProfil;