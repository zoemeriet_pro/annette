import React, { useState } from "react";
import { ActivityIndicator, Alert, Modal, View, Dimensions, Text, StyleSheet, Pressable, SafeAreaView, ScrollView, Image, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";
import * as ImagePicker from 'expo-image-picker';
import { Camera } from 'expo-camera';
import { v4 as uuidv4 } from 'uuid';

// API
import * as firebase from 'firebase'
import { addDefiPhoto } from '../../../api/firebaseMethods'

// Assets
import Images from "../../../../assets/img";

// Components
import Icon from '../../../components/atoms/Icon';
import { TitleBlack } from '../../../components/atoms/Title'
import { Button, ButtonBorder } from "../../../components/atoms/Button";
import { ModalImagePicker } from '../../../components/molecules/Modal'

// Config
import colors from "../../../config/colors";
import typography from "../../../config/typography";
import { ModalCamera } from "../../../components/molecules/ModalCamera";


const ParticipatePhoto = ({data}) => {
  const currentUserUID = firebase.auth().currentUser.uid

  const [modalVisible, setModalVisible] = useState(false)
  const [cameraVisible, setCameraVisible] = useState(false)
  const [selectedImage, setSelectedImage] = useState(null)
  const [uploading, setUploading] = useState(false)
  
  const [cameraRef, setCameraRef] = useState(null)
  const [cameraType, setCameraType] = useState(Camera.Constants.Type.back)

  // TAKE PHOTO

  const takePhotoFromCamera = async() => {
    const {status} = await Camera.requestPermissionsAsync()
    if (status === 'granted') {
      setCameraVisible(true)
      setModalVisible(false)
    } else {
      Alert.alert("Access denied")
    }
  }

  const takePicture = async () => {
    if (cameraRef) {
      const photo = await cameraRef.takePictureAsync()
      setSelectedImage({ localUri: photo.uri })
      setCameraVisible(false)
    }
  }

  const switchCamera = () => {
    if (cameraType === 'back') {
      setCameraType('front')
    } else {
      setCameraType('back')
    }
  }

  // CHOOSE PHOTO

  const choosePhotoFromLibrary = async () => {
    const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }
    const pickerResult = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    })
    if (pickerResult.cancelled === true) {
      return;
    }
    setSelectedImage({ localUri: pickerResult.uri })
    setModalVisible(false)
  }

  // SUBMIT

  const submitPhoto = async () => {
    if (selectedImage) {
      const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.onload = function() {
          resolve(xhr.response)
        }
        xhr.onerror = function() {
          reject(new TypeError('Network request failed'))
        }
        xhr.responseType = 'blob'
        xhr.open('GET', selectedImage.localUri, true)
        xhr.send(null)
      })
      const ref = firebase.storage().ref().child(`photos/${new Date().toISOString()}`)
      const snapshot = ref.put(blob)
      snapshot.on(firebase.storage.TaskEvent.STATE_CHANGES, () => {
        setUploading(true)
      }, (error) => {
        setUploading(false)
        console.log(error)
        blob.close()
        return 
      }, () => {
        snapshot.snapshot.ref.getDownloadURL().then((url) => {
          setUploading(false)
          addDefiPhoto(currentUserUID, data.id, url)
          Actions.pop()
          setTimeout(() => { Actions.refresh({key:Math.random()}) }, 0);
          blob.close()
          return url
        })
      })
    }
  }

  return (
    <SafeAreaView style={styles.page}>
      <ScrollView>
        <View style={styles.container}>

          <View style={styles.header}>
            <TitleBlack text={data.title} />
            <Text style={styles.body}>{data.description}</Text>
          </View>

          {selectedImage !== null
            ? 
            <View>
              <Image
                source={{ uri: selectedImage.localUri }}
                style={styles.thumbnail}
              />
              <View style={styles.actions}>
                <View style={styles.actionLeft}><ButtonBorder text="Changer" onPress={() => {setModalVisible(true)}} /></View>
                <View style={styles.actionRight}><Button text="Confirmer" onPress={submitPhoto}/></View>
                {uploading &&
                  <ActivityIndicator />
                }
              </View>
            </View>
            : <Button text="Poster une photo"  onPress={() => {setModalVisible(true)}}/>
          }
        </View>
      </ScrollView>

      <Image source={Images['participatePhoto']} style={styles.background} />

      <ModalImagePicker
        visible={modalVisible}
        onClose={() => setModalVisible(false)}
        takePhotoFromCamera={takePhotoFromCamera}
        choosePhotoFromLibrary={choosePhotoFromLibrary}
      />

      <ModalCamera 
        visible={cameraVisible}
        cameraType={cameraType}
        reference={reference => {setCameraRef(reference)}}
        takePicture={takePicture}
        switchCamera={switchCamera}
      />
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    marginHorizontal: width > 600 ? 50 : 20,
    paddingBottom: 150,
    paddingTop: 66
  },

  // HEADER
  header: {
    marginTop: 20
  },
  body: {
    color: colors.black,
    fontFamily: typography.regular,
    fontSize: 18,
    marginTop: 10
  },

  // THUMBNAIL
  thumbnail: {
    marginTop: 20,
    width: '100%',
    height: 400,
    resizeMode: "contain",
  },

  // ACTIONS
  actions: {
    display: 'flex',
    flexDirection: "row",
  },
  actionLeft: {
    flex: 1,
    marginRight: 5,
  },
  actionRight: {
    flex: 1,
    marginLeft: 5,
  },

  // BUTTON BACK
  buttonBack: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 40,
    paddingLeft: 20,
    backgroundColor: colors.white,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  buttonBackIcon: {
    color: colors.black,
    fontSize: 25
  },

  // BACKGROUND
  background: {
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: -1,
    width: '100%',
    height: '70%',
    resizeMode: 'stretch',
  },
});

export default ParticipatePhoto;