import React, { useEffect, useState } from "react";
import { ActivityIndicator, Alert, Modal, View, Dimensions, Text, StyleSheet, Pressable, SafeAreaView, ScrollView, Image, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";

// API
import * as firebase from 'firebase'
import { addVotePhoto } from '../../../api/firebaseMethods'

// Assets
import Images from "../../../../assets/img";

// Components
import { TitleBlack } from '../../../components/atoms/Title'
import { ButtonVotePhoto } from "../../../components/atoms/ButtonVote"
import { Button } from "../../../components/atoms/Button";

// Config
import colors from "../../../config/colors";
import typography from "../../../config/typography";


const VotePhoto = ({ data }) => {
  let db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid

  const [loading, setLoading] = useState(true)
  const [dataPost, setDataPost] = useState()
  const [vote, setVote] = useState()

  const submitVote = () => {
    addVotePhoto(currentUserUID, data.id, vote)
    Actions.pop()
    setTimeout(() => { Actions.refresh({key:Math.random()}) }, 0);
  }

  useEffect(() => {
    const docPost = db.collection('participations').where('idDefi', '==', data.id)
    docPost.get().then(querySnapshot => {
      if (querySnapshot.size > 0) {
        var list = []
        querySnapshot.forEach(doc => {
          const { idUser, date, image } = doc.data()
          list.push({ idUser, date, image })
        })
        setDataPost(list)
        setLoading(false)
      }
    })
  }, [])

  if (loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>
      <ScrollView>
        <View style={styles.container}>

          <View style={styles.header}>
            <TitleBlack text={data.title} />
            <Text style={styles.body}>{data.description}</Text>
          </View>

          {dataPost &&
            <View style={styles.list}>
              {dataPost.map((item, index) => (
                <ButtonVotePhoto 
                  image={item.image} 
                  id={item.idUser}
                  selectedImage={vote}
                  onPress={() => setVote(item.idUser)}
                  key={index} 
                />
              ))}
            </View>
          }

        </View>
      </ScrollView>

      {vote &&
        <View style={styles.buttonSubmit}>
          <Button text="Confirmer" onPress={submitVote} />
        </View>
      }

      <Image source={Images['votePhoto']} style={styles.background} />
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    marginHorizontal: width > 600 ? 50 : 20,
    paddingBottom: 150,
    paddingTop: 66
  },

  // HEADER
  header: {
    marginTop: 20
  },
  body: {
    color: colors.black,
    fontFamily: typography.regular,
    fontSize: 18,
    marginTop: 10
  },

  list: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginTop: 20,
  },

  // BUTTON SUBMIT
  buttonSubmit: {
    position: 'absolute',
    bottom: 125,
    left: 0,
    width: '100%',
    paddingHorizontal: width > 600 ? 50 : 20
  },

  // BACKGROUND
  background: {
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: -1,
    width: '100%',
    height: '70%',
    resizeMode: 'stretch',
  },
});

export default VotePhoto;