import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import { Dimensions, StyleSheet, View, ScrollView, SafeAreaView, Text, Alert } from "react-native";

// API
import * as firebase from 'firebase';

// Components
import { TitleBlack } from "../../components/atoms/Title";
import { CardDefi } from "../../components/molecules/CardDefi";
import Topbar from "../../components/molecules/Topbar";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Defis = () => {
  const db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid;
  const docUser = db.collection('users').doc(currentUserUID)
  const docDefis = db.collection('defis')

  const [loading, setLoading] = useState(true)
  const [dataDefisParticipate, setDataDefisParticipate] = useState()
  const [dataDefisVote, setDataDefisVote] = useState()
  const [dataDefisResult, setDataDefisResult] = useState()

  const currentDate = new Date()

  useEffect(() => {
    docDefis.get().then(querySnapshot => {
      var defisParticipate = []
      var defisVote = []
      var defisResult = []
      querySnapshot.forEach(doc => {
        const id = doc.id
        const { type, title, description, points, dateParticipate, dateVote, dateResult } = doc.data()
        if (currentDate > dateParticipate.toDate() && currentDate < dateVote.toDate()) {
          defisParticipate.push({ id, type, title, description, points, dateVote })
        }
        if (currentDate > dateVote.toDate() && currentDate < dateResult.toDate()) {
          defisVote.push({ id, type, title, description, points, dateResult })
        } 
        if (currentDate > dateResult.toDate()) {
          defisResult.push({ id, type, title, description, points, dateResult })
        }
      })
      setDataDefisParticipate(defisParticipate)
      setDataDefisVote(defisVote)
      setDataDefisResult(defisResult)
      setLoading(false)
    })
  }, [])

  if (loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>

      <Topbar title="Les défis" />

      <ScrollView>
        <View style={styles.container}>

          {/* Participation */}
          <View style={styles.section}>
            <TitleBlack text="Participation" />
            {dataDefisParticipate.map((item, index) => (
              <CardDefi key={index} data={item} onPress={() => Actions.defiParticipate({idDefi: item.id})} />
            ))}
          </View>

          {/* Vote */}
          <View style={styles.section}>
            <TitleBlack text="Vote" />
            {dataDefisVote.map((item, index) => (
              <CardDefi key={index} data={item} onPress={() => Actions.defiVote({idDefi: item.id})} />
            ))}
          </View>

          {/* Résultats */}
          <View style={styles.section}>
            <TitleBlack text="Résultats" />
            {dataDefisResult.map((item, index) => (
              <CardDefi key={index} data={item} onPress={() => Actions.defiResult({idDefi: item.id})} />
            ))}
          </View>

        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    paddingBottom: 200,
  },
  section: {
    marginBottom: 50,
    marginHorizontal: width > 600 ? 50 : 20,
  },
});


export default Defis;