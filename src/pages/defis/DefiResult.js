import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import { Dimensions, StyleSheet, View, ScrollView, SafeAreaView, Text, Alert, Image } from "react-native";

// API
import * as firebase from 'firebase';

// Assets
import Images from "../../../assets/img";

// Components
import { Button } from "../../components/atoms/Button";
import { TitleBlue, TitleBlack, Subtitle } from "../../components/atoms/Title";
import Topbar from "../../components/molecules/Topbar";
import { InfoDefi } from "../../components/molecules/InfoDefi";
import { CardPost } from "../../components/molecules/CardPost";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const background = text => {
  if (text == "Photo") {
    return "resultPhoto";
  }
  if (text == "Video") {
    return "resultVideo";
  }
  if (text == "Humeur") {
    return "resultHumeur";
  }
  if (text == "Quiz") {
    return "resultQuiz";
  }
  return colors.white;
};

const DefiResult = ({ idDefi }) => {
  let db = firebase.firestore()
  const docDefi = db.collection('defis').doc(idDefi)

  const [loading, setLoading] = useState(true)
  const [dataDefi, setDataDefi] = useState()
  const [dataPost, setDataPost] = useState()
  const [dataResult, setDataResult] = useState()

  const getMostDuplicated = array => {
    let duplicated = '';
    let max = 0;
    for (let i = 0; i < array.length; i++) {
      let counter = 0;
      for (let j = 1; j < array.length - 1; j++) {
        if (array[i] === array[j]) counter++;
      }
      if (counter > max) {
        duplicated = array[i];
        max = counter;
      }
    }
    return duplicated;
  };

  const getPostResult = (idUser) => {
    const docResult = db.collection('participations').where('idDefi', '==', idDefi).where('idUser', '==', idUser)
    docResult.get().then(querySnapshot => {
      if (querySnapshot.size > 0) {
        querySnapshot.forEach(doc => {
          const { idUser, date, image } = doc.data()
          setDataResult({ idUser, date, image })
        })
      }
    })
  }

  const getResult = () => {
    const docVotes = db.collection('votes').where('idDefi', '==', idDefi)
    docVotes.get().then(querySnapshot => {
      if (querySnapshot.size > 0) {
        var list = []
        querySnapshot.forEach(doc => {
          const { vote } = doc.data()
          list.push(vote)
        })
        getPostResult(getMostDuplicated(list))
      }
    })
  }

  const getPost = () => {
    const docPost = db.collection('participations').where('idDefi', '==', idDefi)
    docPost.get().then(querySnapshot => {
      if (querySnapshot.size > 0) {
        var list = []
        querySnapshot.forEach(doc => {
          const { idUser, date, image } = doc.data()
          list.push({ idUser, date, image })
        })
        setDataPost(list)
      }
    })
  }

  useEffect(() => {
    async function getInfos() {
      await getResult()
      await getPost()

      docDefi.get().then(doc => {
        if (doc.exists) {
          const { type, title, description, points, dateParticipate } = doc.data()
          setDataDefi({ type, title, description, points, dateParticipate })
          setLoading(false)
        } else {
          console.log("Error: No doc defi");
        }
      })
    }
    getInfos()
  }, [])

  

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>

      <Topbar tag={dataDefi.type} />

      <ScrollView>

        <View style={styles.container}>

          {/* Info */}
          <View>
            <InfoDefi data={dataDefi} />
          </View>

          {dataResult &&
            <View style={styles.section}>
              <Text style={styles.subtitle}>La grand gagnant !</Text>
              <CardPost data={dataResult} />
            </View>
          }

          {/* Participations */}
          {dataPost &&
            <View style={styles.section}>
              <Text style={styles.subtitle}>Toutes les participations</Text>
              {dataPost.map((item, index) => (
                <CardPost data={item} key={index} />
              ))}
            </View>
          }

        </View>

      </ScrollView>

      <Image source={Images[background(dataDefi.type)]} style={styles.background} />
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    marginHorizontal: width > 600 ? 50 : 20,
    paddingBottom: 240,
  },
  section: {
    marginTop: 50,
  },
  subtitle: {
    color: colors.black,
    fontFamily: typography.bold,
    fontSize: 25,
    marginTop: 10
  },
  background: {
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: -1,
    width: '100%',
    height: '70%',
    resizeMode: 'stretch',
  }
});


export default DefiResult;