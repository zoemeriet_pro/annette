import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import { Dimensions, StyleSheet, View, ScrollView, SafeAreaView, Text, Alert, Image } from "react-native";

// API
import * as firebase from 'firebase';

// Assets
import Images from "../../../assets/img";

// Components
import { Button } from "../../components/atoms/Button";
import { TitleBlue } from "../../components/atoms/Title";
import Topbar from "../../components/molecules/Topbar";
import { InfoDefi } from "../../components/molecules/InfoDefi";
import { CardPost } from "../../components/molecules/CardPost";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const background = text => {
  if (text == "Photo") {
    return "votePhoto";
  }
  if (text == "Video") {
    return "voteVideo";
  }
  if (text == "Humeur") {
    return "voteHumeur";
  }
  if (text == "Quiz") {
    return "voteQuiz";
  }
  return colors.white;
};

const DefiVote = ({ idDefi }) => {
  let db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid
  const docDefi = db.collection('defis').doc(idDefi)
  const docVote = db.collection('votes').where('idDefi', '==', idDefi).where('idUser', '==', currentUserUID)

  const [loading, setLoading] = useState(true)
  const [dataDefi, setDataDefi] = useState()
  const [dataVote, setDataVote] = useState()
  const [dataPost, setDataPost] = useState()

  const getDataVote = () => {
    docVote.get().then(querySnapshot => {
      if (querySnapshot.size == 1) {
        querySnapshot.forEach(doc => {
          const { vote } = doc.data()
          setDataVote({ vote })
        })
      } 
    })
  }

  const getPost = () => {
    const docPost = db.collection('participations').where('idDefi', '==', idDefi)
    docPost.get().then(querySnapshot => {
      if (querySnapshot.size > 0) {
        var list = []
        querySnapshot.forEach(doc => {
          const { idUser, date, image } = doc.data()
          list.push({ idUser, date, image })
        })
        setDataPost(list)
      }
    })
  }

  useEffect(() => {
    async function getInfos() {
      await getDataVote()
      await getPost()

      docDefi.get().then(doc => {
        if (doc.exists) {
          const id = doc.id
          const { type, title, description, points, dateParticipate } = doc.data()
          setDataDefi({ id, type, title, description, points, dateParticipate })
          setLoading(false)
        } else {
          console.log("Error: No doc defi");
        }
      })
    }
    getInfos()
  }, [])

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>

      <Topbar tag={dataDefi.type} />

      <ScrollView>
        <View style={styles.container}>

          {/* Info */}
          <View>
            {dataVote 
            ? <TitleBlue text="Bravo pour ton vote" />
            : <TitleBlue text="Il est temps de voter" />}
            <InfoDefi data={dataDefi} />
          </View>

          {/* Participations */}
          {dataPost &&
            <View style={styles.section}>
              <Text style={styles.subtitle}>Toutes les participations</Text>
              {dataPost.map((item, index) => (
                <CardPost data={item} key={index} />
              ))}
            </View>
          }

        </View>
      </ScrollView>

      {/* Button Vote */}
      <View style={styles.buttonVote}>
        {dataDefi.type == "Photo" &&
          <Button text="Je vote" onPress={() => Actions.votePhoto({ data: dataDefi })} />
        }
      </View>

      <Image source={Images[background(dataDefi.type)]} style={styles.background} />
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    marginHorizontal: width > 600 ? 50 : 20,
    paddingBottom: 240
  },
  section: {
    marginTop: 50,
  },
  subtitle: {
    color: colors.black,
    fontFamily: typography.bold,
    fontSize: 25,
    marginTop: 10
  },
  buttonVote: {
    position: 'absolute',
    bottom: 190,
    left: 0,
    width: '100%',
    paddingHorizontal: width > 600 ? 50 : 20
  },
  background: {
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: -1,
    width: '100%',
    height: '70%',
    resizeMode: 'stretch',
  }
});


export default DefiVote;