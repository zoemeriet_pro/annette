import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import { Dimensions, StyleSheet, View, ScrollView, SafeAreaView, Text, Image } from "react-native";

// API
import * as firebase from 'firebase';

// Assets
import Images from "../../../assets/img";

// Components
import { Button, ButtonBorder } from "../../components/atoms/Button";
import { TitleBlue, TitleBlack, Subtitle } from "../../components/atoms/Title";
import Topbar from "../../components/molecules/Topbar";
import { InfoDefiParticipate } from "../../components/molecules/InfoDefi";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";
import { CardPost } from "../../components/molecules/CardPost";
import { Participation } from "../../components/molecules/Participation";


const background = text => {
  if (text == "Photo") {
    return "participatePhoto";
  }
  if (text == "Video") {
    return "participateVideo";
  }
  if (text == "Humeur") {
    return "participateHumeur";
  }
  if (text == "Quiz") {
    return "participateQuiz";
  }
  return colors.white;
};

const DefiParticipate = ({ idDefi }) => {
  let db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid
  const docDefi = db.collection('defis').doc(idDefi)
  const docUser = db.collection('users').doc(currentUserUID)
  const docUserDefi = db.collection('participations').where('idUser', '==', currentUserUID).where('idDefi', '==', idDefi)
  const docParticipates = db.collection('participations').where('idDefi', '==', idDefi)

  const [loading, setLoading] = useState(true)
  const [dataDefi, setDataDefi] = useState()
  const [dataUserDefi, setDataUserDefi] = useState()
  const [dataParticipates, setDataParticipates] = useState()

  const getDataUserDefi = () => {
    docUserDefi.get().then(querySnapshot => {
      if (querySnapshot.size == 1) {
        querySnapshot.forEach(doc => {
          const { idUser, date, image } = doc.data()
          setDataUserDefi({ idUser, date, image })
        })
      } 
    })
  }

  const getDataParticipates = () => {
    docParticipates.get().then(querySnapshot => {
      if (querySnapshot.size > 0) {
        var list = []
        querySnapshot.forEach(doc => {
          const { idUser } = doc.data()
          list.push({ idUser })
        })
        setDataParticipates(list)
      } 
    })
  }

  useEffect(() => {
    async function getInfos() {
      await getDataUserDefi()
      await getDataParticipates()

      docDefi.get().then(doc => {
        if (doc.exists) {
          const id = doc.id
          const { type, title, description, points, dateParticipate } = doc.data()
          setDataDefi({ id, type, title, description, points, dateParticipate })
          setLoading(false)
        } else {
          console.log("Error: No doc defi");
        }
      })
    }
    getInfos()
    
  }, [])

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>

      <Topbar tag={dataDefi.type} />

      <ScrollView>
        <View style={styles.container}>

          {/* Participation */}
          <View>
            {dataUserDefi &&
              <TitleBlue text="Bravo pour ta participation !" />
            }
            <InfoDefiParticipate data={dataDefi} />
            {!dataUserDefi &&
              <Button text="Je participe" onPress={() => Actions.participatePhoto({data: dataDefi})} />
            }   
          </View>

          {dataUserDefi &&
            <View style={styles.section}>
              <Text style={styles.subtitle}>Ma participation</Text>
              <CardPost data={dataUserDefi} />
            </View>
          }

          {/* Résultats */}
          {dataParticipates 
          ? 
          <View style={styles.section}>
            <Text style={styles.subtitle}>{dataParticipates.length} {dataParticipates.length > 1 ? "participations" : "participation"}</Text>
            {dataParticipates.map((item, index) => (
              <Participation data={item} key={index} />
            ))}
          </View>
          : 
          <View style={styles.section}>
            <Text style={styles.subtitle}>Pas encore de participation</Text>
          </View>
          }
        </View>
      </ScrollView>

      <Image source={Images[background(dataDefi.type)]} style={styles.background} />
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    marginHorizontal: width > 600 ? 50 : 20,
    paddingBottom: 240
  },
  section: {
    marginTop: 50,
  },
  subtitle: {
    color: colors.black,
    fontFamily: typography.bold,
    fontSize: 25,
    marginBottom: 10,
  },
  background: {
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: -1,
    width: '100%',
    height: '70%',
    resizeMode: 'stretch',
  }
});


export default DefiParticipate;