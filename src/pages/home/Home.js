import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import { Dimensions, StyleSheet, View, ScrollView, SafeAreaView, Text } from "react-native";

// API
import * as firebase from 'firebase';

// Components
import { ButtonBorder } from "../../components/atoms/Button";
import { TitleBlue, TitleBlack, Subtitle } from "../../components/atoms/Title";
import { CardDefiHome } from "../../components/molecules/CardDefi";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Home = () => {
  const db = firebase.firestore()
  const currentUserUID = firebase.auth().currentUser.uid;
  const docUser = db.collection('users').doc(currentUserUID)
  const docDefis = db.collection('defis')

  const [loading, setLoading] = useState(true);
  const [dataUser, setDataUser] = useState()
  const [dataFamily, setDataFamily] = useState()
  const [dataDefis, setDataDefis] = useState()

  const getDataFamily = (familyCode) => {
    const docFamily = db.collection('family').doc(familyCode)
    docFamily.get().then(doc => {
      if (doc.exists) {
        const { goal } = doc.data()
        setDataFamily({ goal })
        setLoading(false)
      } else {
        console.log("Error: No doc family");
      }
    })
  }

  useEffect(() => {
    docDefis.get().then(querySnapshot => {
      var list = []
      querySnapshot.forEach(doc => {
        const id = doc.id
        const { type, title, description, points } = doc.data()
        list.push({ id, type, title, description, points })
      })
      setDataDefis(list)
    })
    docUser.get().then(doc => {
      if (doc.exists) {
        const { firstName, familyCode } = doc.data()
        setDataUser({ firstName, familyCode })
        getDataFamily(familyCode)
      } else {
        console.log("Error: No doc user");
      }
    })
  }, [])

  

  if(loading) {
    return null;
  }

  return (
    <SafeAreaView style={styles.page}>
      <ScrollView>
        <View style={styles.container}>

          {/* Intro */}
          <View style={styles.intro}>
            <TitleBlue text={"Bonjour " + dataUser.firstName + " !"} />
            <Subtitle text="Prêt à te créer des souvenirs aujourd’hui ?" />
          </View>

          {/* Défis en cours */}
          <View style={styles.section}>
            <View style={styles.margin}><TitleBlack text="Défis en cours"/></View>
            <ScrollView
              style={styles.defisList}
              horizontal
              snapToInterval="220"
              decelerationRate="fast"
              snapToAlignment="start"
            >
              {dataDefis.map((item, index) => (
                <CardDefiHome key={index} data={item} onPress={() => Actions.defiParticipate({idDefi: item.id})} />
              ))}
            </ScrollView>
            <View style={styles.margin}><ButtonBorder text="Voir tous les défis" /></View>
          </View>

          {/* Souvenir du jour */}
          <View style={[styles.section, styles.margin]}>
            <TitleBlack text="Souvenir du jour" />
            <ButtonBorder text="Découvrir tous les souvenirs" />
          </View>

          {/* Objectif familial */}
          <View style={[styles.section, styles.margin]}>
            <TitleBlack text="Objectif familial" />
            <View style={styles.card}>
              <Text style={styles.cardTitle}>{dataFamily.goal}</Text>
            </View>
            <View style={styles.card}>
              <Text style={styles.cardTitle}>Objectif du mois</Text>
            </View>
            <View style={styles.card}>
              <Text style={styles.cardTitleBlack}>32%</Text>
              <Text style={styles.cardBodyBold}>de l’ojectif familial a été atteint</Text>
            </View>
            <View style={styles.column}>
              <View style={[styles.card, styles.left]}>
                <Text style={styles.cardTitleBlack}>340</Text>
                <Text style={styles.cardBodyBold}>souvenirs</Text>
              </View>
              <View style={[styles.card, styles.right]}>
                <Text style={styles.cardTitleBlack}>32%</Text>
                <Text style={styles.cardBodyBold}>de l’ojectif familial a été atteint</Text>
              </View>
            </View>
          </View>

        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
  container: {
    paddingBottom: 150,
  },

  intro: {
    marginTop: 30,
    marginHorizontal: width > 600 ? 50 : 20,
  },

  section: {
    marginTop: 50,
  },

  margin: {
    marginHorizontal: width > 600 ? 50 : 20,
  },

  defisList: {
    width: "100%",
    marginLeft: width > 600 ? 50 : 20,
    overflow: 'visible'
  },

  // Column
  column: {
    display: 'flex',
    flexDirection: 'row'
  },
  left: {
    flex: 1,
    marginRight: 5,
  },
  right: {
    flex: 1,
    marginLeft: 5,
  },

  // Card
  card: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 10,
    padding: 20,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  cardTitle: {
    color: colors.blue,
    fontSize: 22,
    fontFamily: typography.bold,
    textAlign: 'center'
  },
  cardTitleBlack: {
    color: colors.black,
    fontSize: 40,
    fontFamily: typography.extrabold,
    textAlign: 'center'
  },
  cardBodyBold: {
    color: colors.black,
    fontSize: 15,
    fontFamily: typography.bold,
    textAlign: 'center'
  }
});


export default Home;