import React from "react";
import { StyleSheet, View, ScrollView, SafeAreaView, Text, Dimensions } from "react-native";

// API
import * as firebase from 'firebase';

// Components
import { ButtonBorder } from "../../components/atoms/Button";
import { TitleBlue, TitleBlack, Subtitle } from "../../components/atoms/Title";
import Topbar from "../../components/molecules/Topbar";

// Config
import colors from "../../config/colors";
import typography from "../../config/typography";

const Souvenirs = () => {
  return (
    <SafeAreaView style={styles.page}>

      <Topbar title="La boîte à souvenirs" />

      <ScrollView>

      </ScrollView>
    </SafeAreaView>
  )
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  page: {
    minHeight: height,
    backgroundColor: colors.background
  },
});


export default Souvenirs;