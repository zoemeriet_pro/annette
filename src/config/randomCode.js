function randomCode() {
  var a = 6,
      b = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      c = '',
      d = 0,
      e = ''+b;
  for (; d < a; d++) {
    c += e[Math.floor(Math.random() * e.length)];
  }
  return c;
}

export default randomCode;