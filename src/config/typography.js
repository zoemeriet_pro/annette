const typography = {
  light: "assistantLight",
  regular: "assistant",
  medium: "assistantMedium",
  bold: "assistantBold",
  extrabold: "assistantExtraBold"
};

export default typography;
