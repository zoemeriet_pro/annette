const colors = {
  black: "#020202",
  white: "#FFFFFF",
  blue: '#1326F6',
  grey: "#959595",
  pink: "#E8488B",
  purple: "#4F4BF1",
  orange: "#F09062",
  background: "#F8F8F8",

  photo: '#FAC7FD',
  video: '#8FD9A8',
  humeur: '#FFDF6B',
  quiz: '#9D92E9'
};

export default colors;
